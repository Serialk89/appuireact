import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import { Meteor } from 'meteor/meteor';
import { createContainer } from 'meteor/react-meteor-data';

import Header from './parts/Header.jsx';
import Sidebar from './parts/Sidebar.jsx';
import Infobox from './widgets/Infobox.jsx';

// App component - represents the whole app
export default class App extends Component {
	componentDidMount(){
		$("#page-content").css({'height':$(window).height()+"px"});
	}
	render() {
		return (
			<div id="page-wrapper">
	    		<div className="preloader">
			        <div className="inner">
			            <div className="preloader-spinner themed-background hidden-lt-ie10"></div>
						<h3 className="text-primary visible-lt-ie10"><strong>Loading..</strong></h3>
			        </div>
			    </div>
	   			<div id="page-container" className="header-fixed-top sidebar-visible-lg-full">
			        <div id="main-container">
			        	<Sidebar />
			        	<Header />
			        	<div id="page-content">
			        		<div className="row">
					        	<Infobox data="100" string="Usuarios" />
					        	<Infobox data="1010" string="Usuarios" />
					        	<Infobox data="1020" string="Usuarios" icon="fa fa-grav" />
					        	<Infobox data="1030" string="Usuarios" />
					        	<Infobox data="1040" string="Usuarios" />
						    </div>
			        	</div>
			        </div>
			    </div>
			</div>
	    )
	}
}
/*
// para la conexion con mongo, se crea un contenedor
App.propTypes = {
	tasks: PropTypes.array.isRequired,
	incompleteCount: PropTypes.number.isRequired,
	currentUser: PropTypes.object,
};
 
export default createContainer(() => {
	Meteor.subscribe('tasks');
	return {
		tasks: Tasks.find({}, { sort: { createdAt: -1 } }).fetch(),
		incompleteCount: Tasks.find({ checked: { $ne: true } }).count(),
		currentUser: Meteor.user(),
	};
}, App);
*/