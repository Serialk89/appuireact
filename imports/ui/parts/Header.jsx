import React, { Component, PropTypes } from 'react';
import { Meteor } from 'meteor/meteor';

export default class Header extends Component{
	render(){
		return(
			<header className="navbar navbar-inverse navbar-fixed-top">
			    <ul className="nav navbar-nav-custom">
			        <li>
			            <a href="javascript:void(0)">
			                <i className="fa fa-ellipsis-v fa-fw animation-fadeInRight" id="sidebar-toggle-mini"></i>
			                <i className="fa fa-bars fa-fw animation-fadeInRight" id="sidebar-toggle-full"></i>
			            </a>
			        </li>
			        <li className="hidden-xs animation-fadeInQuick">
			            <a href=""><strong>Home</strong></a>
			        </li>
			    </ul>
			    <ul className="nav navbar-nav-custom pull-right">
			        <li>
			            <form action="page_ready_search_results.php" method="post" className="navbar-form-custom">
			                <input type="text" id="top-search" name="top-search" className="form-control" placeholder="Search.." />
			            </form>
			        </li>
			        <li>
			            <a href="javascript:void(0)">
			                <i className="gi gi-settings"></i>
			            </a>
			        </li>
			        <li className="dropdown">
			            <a href="javascript:void(0)" className="dropdown-toggle" data-toggle="dropdown">
			                <img src="/img/avatar.jpg" alt="avatar" />
			            </a>
			            <ul className="dropdown-menu dropdown-menu-right">
			                <li className="dropdown-header">
			                    <strong>ADMINISTRATOR</strong>
			                </li>
			            </ul>
			        </li>
			    </ul>
			</header>
		)
	}
}