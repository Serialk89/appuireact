import React, { Component, PropTypes } from 'react';
import { Meteor } from 'meteor/meteor';

export default class Sidebar extends Component {
	render(){
		return(
			<div id="sidebar">
			    <div id="sidebar-brand" className="themed-background">
			        <a href="index.php" className="sidebar-title">
			            <i className="fa fa-cube"></i> <span className="sidebar-nav-mini-hide">REACT <strong>APP</strong></span>
			        </a>
			    </div>
			    <div className="slimScrollDiv">
			    	<div id="sidebar-scroll">
				        <div className="sidebar-content">
				            <ul className="sidebar-nav">
	                            <li>
				                    <a href="index.php" className=" active"><i className="fa fa-user-circle-o sidebar-nav-icon"></i>
				                    	<span className="sidebar-nav-mini-hide">Mi Perfil</span></a>
	                            </li>
	                            <li className="sidebar-separator">
				                    <i className="fa fa-ellipsis-h"></i>
				                </li>
	                        </ul>
				        </div>
			    	</div>
			    </div>
			    <div id="sidebar-extra-info" className="sidebar-content sidebar-nav-mini-hide">
			        <div className="text-center">
			            <small>Created <i className="fa fa-rebel text-danger"></i> <a href="http://goo.gl/vNS3I" target="_blank">by Kirico</a></small><br />
			            <small><span id="year-copy">2017</span> © <a href="http://kirico.cl" target="_blank">React-AppUI Alpha</a></small>
			        </div>
			    </div>
			</div>
		)
	}
}